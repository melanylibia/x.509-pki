## GOALS 

- confidentiality -> acceded by authorized party.

- integrity -> correctnes and completness of information can be verified.

- authenticity -> source of information can be verified by  receiving party.

- non-repudiation -> spurce of information can be verified by any third party.

## Cryptography: Concept and Primitives

-  Confidentiality
    **Encryptation**

    Transforming plaintext (unencrypted data) into ciphertext (encrypted data) using an algorithm.

    **AES**
    Symmetric encryption algorithm, meaning the same key is used for both encryption and decryption.

    **RSA**
    
    Asymmetric encryption algorithm, meaning it uses a pair of keys: a public key for encryption and a private key for decryption.

    **AES and RSA**

    Each has its own strengths and use cases in securing data



- Integrity
    **Cryptographic has functions**
    Hash Functions
    They take an input (or "message") and produce a fixed-size string of bytes.

    **Types**
    MD5, SHA-1, SHA2 and SHA-3.

    SHA-2 and SHA-3 play crucial roles in ensuring data integrity, authentication, and various other cryptographic applications by generating fixed-size hash values from input data.

- Authencity 

  HMAC is typically used when authenticating arbitrary data or messages using cryptographic hash functions and shared secret keys.
  
  GCM is specifically designed for authenticated encryption,

- Non-Repudation

    **Digital signatures**
    Provide a mechanism for achieving non-repudiation. By signing a message with their private key, the sender associates their identity with the message.
    
    **Construction**
    Signature constructed by encrypting the hash using th e private key.

    Verification if signature using public key.


