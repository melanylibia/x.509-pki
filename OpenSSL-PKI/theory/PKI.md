## X509 PKI 

- Architectural model for a system to create, distribute and revke X.509 certificates.

- X.509 certificate, atrusted authority assignsa oblic key toa n end-entity.

- X.509 certificates allow the operator of a sufficiently well implemented X.509 PKI for provider End entitiyes those pillars.

- one pillar is cryptography

- All the process is cleraly because the documnets/certificates are certficate policies and the certification Practice Statement.

 ### Simple Process

 - End-Entities
 request 509.x from a trusted autohrity.

 - Trusted authority
  confirms the identity of requetsing end-entities,
  creates X.509 certificates and published it into a public repository.
 
 - End-Entities
 fetch X.509 certificate of communicationpartner from repository.

 - End-Entities
  amke use of fetched X.509 certificates to secure communication or data transfer.

### Components of that part

- End-Entities 
    -  User 
    - legal companies
    -  service/devices
- Registration Authority(RA)
    - System delegated by CA.
    - 
- Certificate Autohrity (CA)
    - Operates procedures delegated
    - Create,issue an d revoke X.509 certificates.
- Repository 
    - system storing X.509 certificates and CRSL and proving means for access and distribution



### X.509 Certificates

    - With an x.509 certificate, a CA assigns a public key to and end-entity for a certain period.

    - each certificate is digitally signed by the CA. 

    - Such a certificate can then be made public.

    - public key can be authenitcated if CA and the digital signature on the certificate.


## Core Elements 

    - With an X.509 certificate, a CA assigns a public  key to an end-entity for a certain-period.  
    - Ignoring details discussed later on, a certificate has the following 5 core elements.
        - subject
        - Issuer
        - Validity period
        - Subject public key
        - Signature

### Creation

- RA verified the content and CA create the cert and sign the content with the private key and then publish in a repository.

### Validation

- Subject must be real entity 
- Issuer must be a trusted CA
- Validity period
- Revocation status must ckek th status of certificate on CRL also OCSP
- Signature  ckec the sign  must be create with the private key of the issuer.

### Second Validation

- Digital Sgnature
    - Created using the privet signing key of the issuing CA.

    - Created over the contentof the certificate.

    - XX

### Components II

    - Root CA 
        type of CA very superior, normally only issues certificates  to other CAs.

    - Issuing CA
        Certificate issued by a superior CA, this issues certificates for end-entities.

    - Effect: 
        Little activity with RootCA

        High activity of issuing CA. 

### X.509 PKI Trust Model

 - Trust means trust in the accuracy of the binding between the public key and the information contained in a certificate.

 - CA is trusted all other CAs obteined certificate from it are trusted as well. 

 - End-entities trust the Root CA.

### Cetificate and trust validation

 - In general, all entity certificates requires to validate a chain of certificates.
 
 -  Starting with the end-entity certificate, each certificate  in the chain is validated acording to the cryptographic also the public key of the issuer.

 - If this validation succeds for each certificates in the chain, and the last certificate in the chain is the certificate of a trusted CA.

