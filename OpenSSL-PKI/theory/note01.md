## OpenSSL PKI 
---

What is X.509 PKI?
Is a security architecture that uses well-established cryptographic mechanisms to support use-cases like email protection and web server authentication. 

---

What is X.509?
X.509 is a standard that defines the format for public key certificates. 
These certificates to authenticate the identity of users, devices, or services, and to establish secure communication channels.

---

An example: 

Transport Layer Security (TLS) is a protocol used to secure communication over a computer network. TLS also relies on X.509 certificates for authenticating the identities of servers and, optionally, clients.
 
---

Public Key Infrastructure (PKI)

Restaurant kitchen:
the restaurant kitchen provides the space and resources for cooking.

PKI:
Provides the framework and infrastructure for managing digital certificates.

---

Certificate Authority (CA):

Restaurant kitchen:
The head chef they have the authority to approve and certify the ingredients and dishes. 

CA:
The CA issues certificates and Certificate Revocation Lists (CRLs) to authenticate digital identities and manage security.

---

Registration Authority (RA):

Restaurant kitchen:
Sous chef who assists the head chef handle the initial enrollment

RA:
They handle the initial enrollment and paperwork required for the PKI process. Sometimes perform tasks similar to the CA, only of are smaller operations.

---
Certificate:

Restaurant kitchen:
Finished dishes served to the customers. Each dish has its unique identity and quality assurance.

Certificates:
Contain public keys and identification information signed by the CA.

---

Certificate Signing Request (CSR):

Restaurant kitchen:

Customer placing an order for a specific dish. Contains the details (ingredients).

CSR:
CA processes this request to create the certificate CSR according to the specifications provided.

---

Certificate Revocation List (CRL):

Restaurant kitchen:
Imagine the CRL as a list of ingredients that have gone bad or expired. 
CRL:
List issued by the CA at regular intervals, containing details of certificates that are no longer valid, either due to expiration or revocation.

---

Certification Practice Statement (CPS):

Restaurant kitchen:

Recipe book followed in the kitchen. 
Book ensures consistency in cooking techniques.

CPS:
It describes the structure and processes of the CA, ensuring consistency and compliance with industry standards, 

---

Resources: 

[PKI openssl](https://pki-tutorial.readthedocs.io/en/latest/#pki-concepts)

