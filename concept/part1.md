## Introducción PKI y X.509

### Bases

### ¿Qué es la criptografía de clave pública?

Tambien conocida como criptografía asimétrica,

### ¿Qué es una firma digital?

- Conjunto de datos asociados a un mensaje que permite asegurar la identidad del firmante y la integridad del mensaje

- Proposito, establishes the owner’s identity.

- To verify the digital signature, we need the signing authority’s public key.

### Certificados digitales

An electronic document used to identify an individual, server, company or some other entity. It contains the public key and is signed with the private key to verify that it belongs to the sender.

### Tipos de certificados

 certificado autofirmado.
 certificado firmado por una entidad de certificación.
 certificado de sitio compartido.

### Infraestructura de clave pública

Con el fin de asegurarse de que una clave pública que tiene fue realmente creado por esa persona específica, necesita algún tipo de tercero de confianza.
Conjunto de tecnologías y estándares para gestionar la Creación, almacenamiento y distribución de certificados a las Autoridades de Certificación (CA).
Uso de los certificados X.509.

### Revocación de certificados

El intervalo de renovación del certificado se basa en el análisis de riesgos pérdidas.
El intervalo de renovación de X.509 suele ser de un año.
La revocación proporciona un medio para que un certificado deje de ser válido antes de su fecha de finalización de validez

### Solicitud de certificados

• Lista de revocación de certificados (CRL)
• Una lista con marca de tiempo de los certificados revocados que es Firmado por una autoridad de certificación
• Equivalente a la lista negra de tarjetas de crédito de la década de 1970 cuadernillos que se basaban en cheques anteriores Listas negras.
• Cómo funciona:
• Obtener certificado
• Obtener lista de revocación de certificados (CRL)
• Comprobar el certificado con CRL
• Comprobar la firma mediante certificado


### Solicitud de certificados

El Protocolo de estado de certificados en línea (OCSP) consulta de la CA emisora si un certificado determinado sigue siendo válido.
• Los valores de estado devueltos son "bueno", "revocado" o "Desconocido"
• En lugar de obtener una CRL de todo, el proceso con OCSP envía una respuesta CRL/OCSP para Certificados específicos

### Gestión de claves y certificados

imagen

### Usos de los certificados en aplicaciones de Internet


• Utilizado por los servidores para la seguridad de las conexiones entre servidores y clientes SSL/TLS.
• Correo de privacidad mejorada (PEM) o extensiones de correo de Internet seguro/multipropósito (S/MIME)
• Utiliza certificados digitales para firmas digitales y para el intercambio de claves para cifrar y descifrar mensajes Redes privadas virtuales (VPN).
• Transacción electrónica segura se utilizan para los titulares de tarjetas para permitir conexiones privadas seguras entre los titulares de tarjetas. comerciantes y bancos.

### What Are X.509 Certificates?
• X.509 public key infrastructure standard to verify that a public key belongs to the user, computer or service identify contained within the certificate.
•When a certificate is generated, a private key is also produced, but this private key is not stored inside
the x.509 certificate. Only the public key resides in the x.509 certificate.
– The private key resides in the repository of the end-entity that is represented with the certificate.
– The private key is used for signing certificates and decrypt data that has been encrypted with the
public key. The private key may also be used to encrypt data.
