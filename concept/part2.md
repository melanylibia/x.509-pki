# Autenticación mediante certificados digitales para una VPN

## PKI, SSL y VPN

### VPN

VPN significa Red Privada Virtual
- Una LAN Ethernet que se ejecuta en mi edificio
- Wifi con control de acceso
- Una puerta cerrada con llave y paredes gruesas pueden ser control de acceso
- Una fibra óptica que se ejecuta entre dos edificios

### VPN Tunnels Protocol Stack

– IPsec/IKE
– GRE/L2TP
– SSL

### Tarjeta

Implementa una infraestructura de seguridad basada en certificados utilizando OpenSSL y la utiliza para asegurar tanto los extremos del cliente como del servidor de OpenVPN.

### OpenSSL
OpenSSL es principalmente una biblioteca de funciones criptográficas que proporciona una extensa API de criptografía a los programadores. Sin embargo, también incluye una herramienta de shell que expone esa API a los usuarios y scripts por lotes. Inicia el shell escribiendo openssl en la línea de comandos. Desde allí, puedes escribir comandos en el prompt OpenSSL>.

### El Archivo de Configuración de OpenSSL

Luego de instalar una Autoridad de Certificación OpenSSL, necesitamos proporcionar un archivo de configuración maestro.

especifica información adicional que puede aparecer en el certificado para aumentar la seguridad y ayudar a las aplicaciones que trabajan con ellos.

### El Certificado Raíz de la Autoridad de Certificación

Con la estructura de directorios en su lugar, podemos crear nuestro certificado raíz autofirmado. Este certificado se ubicará en la parte superior de nuestra jerarquía de confianza. Lo usaremos para firmar todo el resto. Para nuestro tutorial, solo usaremos un único nivel de confianza, pero un diseño más robusto probablemente usaría más.

### Certificados de Usuario

podemos crear certificados para todos los clientes a quienes queremos dar acceso VPN. Esto evita que la clave privada generada con la solicitud sea cifrada y protegida por contraseña.

### Marco de Pruebas OpenSSL

Las aplicaciones seguras que utilizan certificados pueden ser difíciles de depurar, especialmente si no estás seguro de dónde puede existir un problema potencial. El cliente y servidor de pruebas de OpenSSL permiten a un administrador aislar más fácilmente los problemas a una aplicación de red o su infraestructura de certificados de soporte.
